import 'dart:convert';

import 'package:Ecocup/models/Globals.dart';
import 'package:Ecocup/services/RestService.dart';
import 'package:Ecocup/services/RouteService.dart';
import 'package:Ecocup/ui/Users/ChooseLoginPage.dart';
import 'package:Ecocup/ui/Dialogs/ReplaceCodeDialog.dart';
import 'package:Ecocup/ui/Users/Home.dart';
import 'package:Ecocup/ui/Users/Login.dart';
import 'package:Ecocup/ui/Users/Map.dart';
import 'package:Ecocup/ui/Users/Notifications.dart';
import 'package:Ecocup/ui/Users/Scanner.dart';
import 'package:Ecocup/ui/Users/Stores.dart';
import 'package:Ecocup/ui/Users/UserProfile.dart';
import 'package:Ecocup/ui/Users/placeholder_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'models/NotificationDto.dart';

void main() {
  runApp(EcocupApp());
}

class EcocupApp extends StatelessWidget {
  // This widget is the root of your application.
  bool _isFirstTime = true;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Ecocup',
      theme: ThemeData(
        primarySwatch: Colors.lightBlue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        scaffoldBackgroundColor: Colors.white,
      ),
      home: determineLandingScreen(context),
    );
  }

  Widget determineLandingScreen(BuildContext context) {
    return FutureBuilder(
      future: isFirstRun(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          if (_isFirstTime == true)
            return ChooseLoginPage();
          else
            return HomePage();
        } else
          return Container(); // or some other widget
      },
    );
  }

  Future<bool> isFirstRun() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _isFirstTime = (prefs.getBool('isFirstTime') ?? true);
    return _isFirstTime;
  }
}

class HomePage extends StatefulWidget {
  HomePage({Key key, this.title}) : super(key: key);
  final String title;
  bool showBadgePublic = false;

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _currentIndex = 0;
  int index = 0;

  int user_id = 0;
  String baseUri = "";
  String name = "";
  String access_token = "";
  List<NotificationDto> _notifications = [];

  final List<Widget> _children = [
    Home(),
    MapScreen(),
    ScanScreen(),
    Notifications(),
    UserProfile()
  ];

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  void initState() {
    super.initState();
    _loadCommons();
  }

  Future<void> _loadCommons() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      user_id = prefs.getInt('user_id');
      name = prefs.getString('name');
      access_token = prefs.getString('access_token');
    });
    _loadNotifications();
  }

  Future<List<NotificationDto>> _loadNotifications() async {
    baseUri = await RouteService().getRoute(this.context, "BaseUri");
    String notificationsRoute =
        await RouteService().getRoute(this.context, "GetUserNotifications");
    Map<String, String> body = new Map();
    body.addAll({
      'access_token': access_token,
      'user_id': user_id.toString(),
    });
    String notificationsResponse =
        await RestService().doPostRequest(baseUri + notificationsRoute, body);
    var list = json.decode(notificationsResponse) as List;
    _notifications = list.map((i) => NotificationDto.fromJson(i)).toList();

    for (int i = 0; i < _notifications.length; i++) {
      if (_notifications[i].seen == "false") showBadge = true;
      print("showBadge " + showBadge.toString());
    }
    return Future.value(_notifications);
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        iconTheme: IconThemeData(color: Colors.white),
        title: const Text('Ecocup', style: TextStyle(color: Colors.white)),
        backgroundColor: Colors.blue,
        actions: <Widget>[
          Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () {
                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return ReplaceCodeDialog(
                          title: "Earn Rewards",
                          descriptions:
                              "Visit RePlace | Resolution app to play and earn a code for your rewards",
                          text: "Done",
                        );
                      });
                },
                child: Icon(
                  Icons.code,
                  color: Colors.white,
                ),
              )),
          Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () {
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) =>
                              ChooseLoginPage()));
                },
                child: Icon(
                  Icons.logout,
                  color: Colors.white,
                ),
              )),
        ],
      ),
      body: _children[_currentIndex],
      bottomNavigationBar: new Stack(
        overflow: Overflow.visible,
        alignment: new FractionalOffset(.5, 1.0),
        children: [
          new BottomNavigationBar(
            onTap: onTabTapped,
            type: BottomNavigationBarType.fixed,
            currentIndex: _currentIndex,
            backgroundColor: Colors.white,
            items: [
              BottomNavigationBarItem(
                icon: new Icon(Icons.home),
                title: new Text('Home'),
              ),
              BottomNavigationBarItem(
                  icon: Icon(Icons.store_mall_directory),
                  title: Text('Stores')),
              BottomNavigationBarItem(icon: Icon(null), title: Text('')),
              BottomNavigationBarItem(
                  icon: new Stack(
                    children: <Widget>[
                      new Icon(Icons.notifications),
                      showBadge == true
                          ? new Positioned(
                              right: 0,
                              child: new Container(
                                padding: EdgeInsets.all(1),
                                decoration: new BoxDecoration(
                                  color: Colors.red,
                                  borderRadius: BorderRadius.circular(6),
                                ),
                                constraints: BoxConstraints(
                                  minWidth: 12,
                                  minHeight: 12,
                                ),
                              ),
                            )
                          : new Text("")
                    ],
                  ),
                  title: Text('Notifications')),
              BottomNavigationBarItem(
                icon: new Icon(Icons.person),
                title: new Text('Profile'),
              )
            ],
          ),
          new Padding(
            padding: const EdgeInsets.only(bottom: 20.0),
            child: new FloatingActionButton(
              onPressed: () => Navigator.push(
                  context, MaterialPageRoute(builder: (_) => ScanScreen())),
              child: new Icon(Icons.add),
              foregroundColor: Colors.white,
            ),
          ),
        ],
      ),
    );
  }
}
