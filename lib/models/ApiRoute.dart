class ApiRoute{
  String _name,_route;

  ApiRoute(this._name, this._route);

  get route => _route;

  set route(value) {
    _route = value;
  }

  String get name => _name;

  set name(String value) {
    _name = value;
  }
}