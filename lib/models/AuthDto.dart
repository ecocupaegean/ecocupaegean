class AuthDto{
  String _status,_error_code;

  String get status => _status;

  set status(String value) {
    _status = value;
  }

  AuthDto(this._status, this._error_code);

  get error_code => _error_code;

  set error_code(value) {
    _error_code = value;
  }

  factory AuthDto.fromJson(Map<String, dynamic> json) {
    return AuthDto(
      json['status'] as String,
      json['title'] as String
    );
  }
}