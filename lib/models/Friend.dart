import 'dart:convert';
import 'package:meta/meta.dart';

class Friend {
  Friend(
      {
      //@required this.avatar,
      @required this.id,
      @required this.name,
      @required this.email,
      @required this.country,
      @required this.imageUrl});

  //final String avatar;
  final String id;
  final String name;
  final String email;
  final String country;
  final String imageUrl;

  factory Friend.fromJson(Map<String, dynamic> json) => Friend(
      id: json["id"],
      name: json["name"],
      email: json["email"],
      country: json["country"],
      imageUrl: "");

  Map<String, dynamic> toJson() =>
      {"id": id, "name": name, "email": email, "country": country};
}
