import 'dart:convert';
import 'package:meta/meta.dart';

class NotificationDto {
  NotificationDto({
    //@required this.avatar,
    @required this.id,
    @required this.title,
    @required this.description,
    @required this.action_required,
    @required this.seen,
  });

  //final String avatar;
  final String id;
  final String title;
  final String description;
  final String action_required;
  final String seen;

  factory NotificationDto.fromJson(Map<String, dynamic> json) => NotificationDto(
      id: json["id"],
      title: json["title"],
      description: json["description"],
      action_required: json["action_required"],
      seen: json["seen"]
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "title": title,
    "description": description,
    "action_required": action_required,
    "seen": seen
  };
}