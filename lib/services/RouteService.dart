import 'dart:io';
import 'package:Ecocup/models/ApiRoute.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:xml/xml.dart';

class RouteService{

   Future<List<ApiRoute>> _parseRoutes(BuildContext context) async{
    final file = await DefaultAssetBundle.of(context).loadString("assets/routes.xml");
    final document = XmlDocument.parse(file);
    var routes = document.findAllElements("Route");
    List<ApiRoute> mappedRoutes = routes.map((e) => ApiRoute(e.findElements("Name").first.text, e.findElements("ApiRoute").first.text)).toList();
    return mappedRoutes;
  }

   Future<String> getRoute(BuildContext context,String route) async{
     List<ApiRoute> routes = await _parseRoutes(context);
     for(int i=0; i<routes.length; i++){
       if(routes[i].name == route){
         return routes[i].route;
       }
     }
     return " ";
   }
}