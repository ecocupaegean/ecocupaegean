import 'dart:math';

import 'package:Ecocup/services/RestService.dart';
import 'package:Ecocup/services/RouteService.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:flutter/services.dart';
import 'dart:async';
import 'dart:typed_data';
import 'dart:ui';
import 'dart:io';
import 'package:flutter/rendering.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class GenerateScreen extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => GenerateScreenState();
}

class GenerateScreenState extends State<GenerateScreen> {

  static const double _topSectionTopPadding = 20.0;
  static const double _topSectionBottomPadding = 20.0;
  static const double _topSectionHeight = 100.0;

  GlobalKey globalKey = new GlobalKey();
  String _dataString = "Hello from this QR";
  String _inputErrorText;
  final TextEditingController _textController =  TextEditingController();

  int store_id = -1;
  var access_token = "";
  var name = "";
  int barcode = -1;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _loadCommons();
  }

  Future<String> _loadCommons() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      var random = new Random();
      int randInt = random.nextInt(20);
      _dataString = (store_id * randInt).toString();
      barcode = int.parse(_dataString);

      store_id = prefs.getInt('store_id');
      name = prefs.getString('name');
      access_token = prefs.getString('access_token');
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _contentWidget(),
    );
  }

  Future<String> updateQRCode() async{
    Map<String,String> body = new Map();
    body.addAll({
      'store_id': store_id.toString(),
      'barcode': barcode.toString(),
    });

    print("store id " + store_id.toString());

    print("GENERATED BARCODE");
    print(barcode);
    String baseUri = await RouteService().getRoute(this.context,"BaseUri");
    String updateQRCodeRoute = await RouteService().getRoute(this.context,"UpdateQRCode");
    String response = await RestService().doPostRequest(baseUri+updateQRCodeRoute, body);

    print("response");
    print(response);
    return response;
  }

  _contentWidget() {
    final bodyHeight = MediaQuery.of(context).size.height - MediaQuery.of(context).viewInsets.bottom;
    return  Container(
      color: const Color(0xFFFFFFFF),
      child:  Column(
        children: <Widget>[
          Container(
            child:  Center(
              child: Padding(
                padding: EdgeInsets.only(top: 120),
                child: RepaintBoundary(
                key: globalKey,
                child: QrImage(
                  data: _dataString,
                  size: 0.40 * bodyHeight,
                ),
              ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              top: 10.0,
              left: 20.0,
              right: 10.0,
              bottom: _topSectionBottomPadding,
            ),
            child:  Container(
              height: 150.0,
              child:  Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 0.0),
                    child:  OutlinedButton(
                      onPressed:(){
                        setState((){
                          var random = new Random();
                          int randInt = random.nextInt(20);
                          _dataString = (store_id * randInt).toString();
                          barcode = int.parse(_dataString);
                        });
                        updateQRCode();
                      },
                      style: ButtonStyle(
                        shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0))),
                      ),
                      child: const Text("GENERATE NEW",style: TextStyle(color: Colors.black, fontSize: 20.0)),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}