import 'package:Ecocup/services/RestService.dart';
import 'package:Ecocup/services/RouteService.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

class BusinessHome extends StatefulWidget{
  BusinessHome({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _BusinessHomeState createState() => _BusinessHomeState();
}

class _BusinessHomeState extends State<BusinessHome> {
  int store_id = 0;
  String name = "";
  String access_token = "";

  String contribWater="0",contribTrees="0",contribCo2="0",contribMoNH="0";
  String contribSingleUseCupsSaved="0";

  String contributionsResponse = "";

  @override
  void initState(){
    super.initState();
    _loadCommons();
  }

  Future<String> _loadCommons() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      store_id = prefs.getInt('store_id');
      name = prefs.getString('name');
      access_token = prefs.getString('access_token');
    });
  }

  Future<String> getStoreContributions() async{
    Map<String,String> body = new Map();
    body.addAll({
      'store_id': store_id.toString(),
    });

    String baseUri = await RouteService().getRoute(this.context,"BaseUri");
    String storeContributionsRoute = await RouteService().getRoute(this.context,"StoresContributionsRoute");
    contributionsResponse = await RestService().doPostRequest(baseUri+storeContributionsRoute, body);
    print(contributionsResponse);
    try {
      Map<String, dynamic> parsedAContributionsResponse = jsonDecode(contributionsResponse);
      contribSingleUseCupsSaved = parsedAContributionsResponse['sucs'];
      contribWater = (parsedAContributionsResponse['water'] ?? 0);
      contribTrees = (parsedAContributionsResponse['trees'] ?? 0);
      contribCo2 = (parsedAContributionsResponse['co2'] ?? 0);
      contribMoNH = (parsedAContributionsResponse['monh'] ?? 0);
    } catch (e) {
      contribSingleUseCupsSaved = "0";
      contribWater = "0";
      contribTrees = "0";
      contribCo2 = "0";
      contribMoNH = "0";
    }
    return contributionsResponse;
  }

  Widget homeWidget(BuildContext context) {
    return FutureBuilder(
      future: getStoreContributions(),
      // ignore: missing_return
      builder: (context, snapshot) {
        if (snapshot.hasData || snapshot.connectionState == ConnectionState.done) {
          return
            SafeArea(
                child: SingleChildScrollView (
                  child:Column(
                    children: [
                      SizedBox(
                        height: 30,
                      ),
                      Padding(
                        padding: EdgeInsets.all(8),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Card(
                              child: Column(children:[
                                Container(
                                  width: MediaQuery. of(context). size. width-30,
                                  height: 90,
                                  child: Image.asset('assets/images/cup.png'),
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                Text(
                                    "Single use cups saved",
                                    style: TextStyle(
                                        fontSize: 18.0,
                                        color:Colors.blueGrey,
                                        fontWeight: FontWeight.w400
                                    )
                                ),
                                SizedBox(
                                  height: 7,
                                ),
                                Padding(
                                    padding: EdgeInsets.only(bottom: 10),
                                    child: Text(
                                      contribSingleUseCupsSaved,
                                      style: TextStyle(
                                          color: Colors.blueGrey,
                                          fontSize: 18.0,
                                          fontWeight: FontWeight.w300
                                      ),)
                                ),
                              ]),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "Your environmental contributions"
                            ,style: TextStyle(
                              fontSize: 15.0,
                              color:Colors.black,
                              fontWeight: FontWeight.w300
                          ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Expanded(
                              child: Card(
                                child:
                                Column(
                                    children: [
                                      Container(
                                        width: 50,
                                        height: 90,
                                        child: Image.asset('assets/images/waterdrops.png'),
                                      ),
                                      SizedBox(
                                        height: 15,
                                      ),
                                      Text("Water",
                                        style: TextStyle(
                                            color: Colors.blueGrey,
                                            fontSize: 18.0,
                                            fontWeight: FontWeight.w400
                                        ),),
                                      SizedBox(
                                        height: 7,
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(bottom: 10),
                                        child: Text(
                                          contribWater + " Lt",
                                          style: TextStyle(
                                              color: Colors.blueGrey,
                                              fontSize: 18.0,
                                              fontWeight: FontWeight.w300
                                          ),),
                                      ),

                                    ]
                                ),
                              ),
                            ),
                            Expanded(
                              child:
                              Card(
                                child: Column(children:[
                                  Container(
                                    width: 50,
                                    height: 90,
                                    child: Image.asset('assets/images/tree.png'),
                                  ),
                                  SizedBox(
                                    height: 15,
                                  ),
                                  Text(
                                    "Trees",
                                    style: TextStyle(
                                        color: Colors.blueGrey,
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.w400
                                    ),),
                                  SizedBox(
                                    height: 7,
                                  ),
                                  Padding(
                                      padding: EdgeInsets.only(bottom: 10),
                                      child: Text(
                                        contribTrees + " trees",
                                        style: TextStyle(
                                            color: Colors.blueGrey,
                                            fontSize: 18.0,
                                            fontWeight: FontWeight.w300
                                        ),)
                                  ),
                                ]),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(8),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Expanded(
                              child: Card(
                                child: Column( children: [
                                  Container(
                                    width: 50,
                                    height: 90,
                                    child: Image.asset('assets/images/co2.png'),
                                  ),
                                  SizedBox(
                                    height: 15,
                                  ),
                                  Text(
                                    "CO2",
                                    style: TextStyle(
                                        color: Colors.blueGrey,
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.w400
                                    ),),
                                  SizedBox(
                                    height: 7,
                                  ),
                                  Padding(
                                      padding: EdgeInsets.only(bottom: 10),
                                      child:  Text(
                                        contribCo2 + " kg",
                                        style: TextStyle(
                                            color: Colors.blueGrey,
                                            fontSize: 18.0,
                                            fontWeight: FontWeight.w300
                                        ),)
                                  ),
                                ],
                                ),),
                            ),
                            Expanded(
                              child:
                              Card(
                                child: Column(children: [
                                  Container(
                                    width: 50,
                                    height: 90,
                                    child: Image.asset('assets/images/planetearth.png'),
                                  ),
                                  SizedBox(
                                    height: 15,
                                  ),
                                  Text(
                                    "Natural habitat",
                                    style: TextStyle(
                                        color: Colors.blueGrey,
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.w400
                                    ),),
                                  SizedBox(
                                    height: 7,
                                  ),
                                  Padding(
                                      padding: EdgeInsets.only(bottom: 10),
                                      child: Text(
                                        contribMoNH + " m\u00b2",
                                        style: TextStyle(
                                            color: Colors.blueGrey,
                                            fontSize: 18.0,
                                            fontWeight: FontWeight.w300
                                        ),)
                                  ),
                                ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 50,
                      ),
                    ],
                  ),
                )
            );
        }
        else if(snapshot.connectionState == ConnectionState.waiting){
          return Center(
            child: CircularProgressIndicator(valueColor: new AlwaysStoppedAnimation<Color>(Colors.orange),),
          );
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return homeWidget(context);
  }
}
