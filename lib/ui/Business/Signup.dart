import 'dart:math';
import 'dart:convert';
import 'package:Ecocup/services/RestService.dart';
import 'package:Ecocup/services/RouteService.dart';
import 'package:Ecocup/services/ValidatorService.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_place_picker/google_maps_place_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'Login.dart';

class BusinessSignUpPage extends StatefulWidget {
  BusinessSignUpPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _BusinessSignUpPageState createState() => _BusinessSignUpPageState();
}

class _BusinessSignUpPageState extends State<BusinessSignUpPage> {
  final _formKey = GlobalKey<FormState>();

  int store_id = -1;
  String token_response = " ";
  String signup_response = " ";
  String token = " ";
  String email = "";
  String password = "";
  String name = "";
  String country = "";
  String phone = "";
  String latitude = "";
  String longtitude = "";
  String _dataString = "Hello from this QR";
  int barcode = -1;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _loadCommons();
  }

  Future<String> _loadCommons() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      var random = new Random();
      int randInt = random.nextInt(20);
      _dataString = (store_id * randInt).toString();
      barcode = int.parse(_dataString);
      //updateQRCode();
    });
  }

  Future<String> generateToken() async{
    Random random = new Random();
    store_id = random.nextInt(15000000);
    Map<String,String> body = new Map();
    body.addAll({
      "user_id": store_id.toString()
    });
    String base_uri_route = await RouteService().getRoute(this.context,"BaseUri");
    String generate_token_route = await RouteService().getRoute(this.context,"GenerateToken");
    String api_token_response = await RestService().doPostRequest(base_uri_route + generate_token_route, body);

    return api_token_response;
  }

  Future<String> register() async{
    Map<String,String> body = new Map();
    print(email + name + country + phone + password);
    body.addAll({
      "name":name,
      "country":country,
      "phone":phone,
      "email":email,
      "password":password,
      "store_id": store_id.toString(),
      "longtitude": longtitude,
      "latitude": latitude
    });

    String base_uri_route = await RouteService().getRoute(this.context,"BaseUri");
    String signup_route = await RouteService().getRoute(this.context,"BusinessSignupRoute");
    String api_signup_response = await RestService().doPostRequest(base_uri_route + signup_route, body);

    return api_signup_response;
  }

  static final kInitialPosition = LatLng(40.619936, 22.968489);

  Future<String> updateQRCode() async{
    Map<String,String> body = new Map();
    body.addAll({
      'store_id': store_id.toString(),
      'barcode': barcode.toString(),
    });

    print(store_id);

    print("GENERATED BARCODE");
    print(barcode);
    String baseUri = await RouteService().getRoute(this.context,"BaseUri");
    String updateQRCodeRoute = await RouteService().getRoute(this.context,"UpdateQRCode");
    String response = await RestService().doPostRequest(baseUri+updateQRCodeRoute, body);

    print("response");
    print(response);
    return response;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          iconTheme: IconThemeData(
              color: Colors.white
          ),
          title: const Text('Ecocup', style: TextStyle(
              color: Colors.black
          )),
          backgroundColor: Colors.white,
        ),
        body: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 60.0),
                  child: Center(
                    child: Container(
                        width: 200,
                        height: 150,
                        child: Image.asset('assets/images/ecocup-orange.png')),
                  ),
                ),
                Padding(
                  //padding: const EdgeInsets.only(left:15.0,right: 15.0,top:0,bottom: 0),
                  padding: EdgeInsets.only(left:15.0,right: 15.0,top:50,bottom: 0),
                  child: TextFormField(
                      decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          enabledBorder: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(25.0),
                            borderSide:  BorderSide(color: Colors.orange ),
                          ),
                          focusedBorder: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(25.0),
                            borderSide:  BorderSide(color: Colors.orange ),

                          ),
                          labelText: 'Email',
                          labelStyle: new TextStyle(color: Colors.orange),
                          hintText: 'yourname@gmail.com'),
                      validator: (value){
                        bool isValid = ValidatorService().validateEmail(value);
                        //if(!isValid) return 'Please enter a valid email';
                        //else email = value;
                        email = value;
                        return null;
                      }
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 15.0, right: 15.0, top: 15, bottom: 0),
                  //padding: EdgeInsets.symmetric(horizontal: 15),
                  child: TextFormField(
                    obscureText: true,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        enabledBorder: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(25.0),
                          borderSide:  BorderSide(color: Colors.orange ),
                        ),
                        focusedBorder: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(25.0),
                          borderSide:  BorderSide(color: Colors.orange ),

                        ),
                        labelText: 'Password',
                        labelStyle: new TextStyle(color: Colors.orange),
                        hintText: 'Enter your password'),
                    validator: (value){
                      password = value;
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 15.0, right: 15.0, top: 15, bottom: 0),
                  //padding: EdgeInsets.symmetric(horizontal: 15),
                  child: TextFormField(
                    decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        enabledBorder: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(25.0),
                          borderSide:  BorderSide(color: Colors.orange ),
                        ),
                        focusedBorder: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(25.0),
                          borderSide:  BorderSide(color: Colors.orange ),

                        ),
                        labelText: 'Name',
                        labelStyle: new TextStyle(color: Colors.orange),
                        hintText: 'Enter your name'),
                    validator: (value){
                      name = value;
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 15.0, right: 15.0, top: 15, bottom: 15),
                  //padding: EdgeInsets.symmetric(horizontal: 15),
                  child: TextFormField(
                    decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        enabledBorder: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(25.0),
                          borderSide:  BorderSide(color: Colors.orange ),
                        ),
                        focusedBorder: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(25.0),
                          borderSide:  BorderSide(color: Colors.orange ),

                        ),
                        labelText: 'Phone',
                        labelStyle: new TextStyle(color: Colors.orange),
                        hintText: 'Enter your phone'),
                    validator: (value){
                      phone = value;
                      return null;
                    },
                  ),
                ),
                Container(
                  height: 50,
                  width: 250,
                  decoration: BoxDecoration(
                      color: Colors.white, borderRadius: BorderRadius.circular(20),border: Border.all(color: Colors.black)),
                  child: FlatButton(
                    onPressed: () => {
                    Navigator.pushReplacement(context, MaterialPageRoute(
                    builder: (context) => PlacePicker(
                    apiKey: "AIzaSyCHflU7NHK1C6hiDusTd_Lyw2Kz2a0FRcM",   // Put YOUR OWN KEY here.
                    onPlacePicked: (result) {
                      longtitude = result.geometry.location.lng.toString();
                      latitude = result.geometry.location.lat.toString();
                      Navigator.of(context).pop();
                    },
                    initialPosition: kInitialPosition,
                    useCurrentLocation: true,
                    ),
                    ),
                    )
                    },
                    child: Text(
                      'Choose Location',
                      style: TextStyle(color: Colors.black, fontSize: 25),
                    ),
                  ),
                ),
                SizedBox(
                  height:15
                ),
                Container(
                  height: 50,
                  width: 250,
                  decoration: BoxDecoration(
                      color: Colors.blue, borderRadius: BorderRadius.circular(20)),
                  child: FlatButton(
                    color: Colors.orange,
                    onPressed: () {
                      if(_formKey.currentState.validate()){
                        //Generate token for user
                        if(longtitude != "" && latitude != "") {
                          generateToken().then((value) {
                            token_response = value;
                            Map<String,
                                dynamic> parsedTokenResponse = jsonDecode(
                                token_response);
                            if (parsedTokenResponse['token'] != "") {
                              //Register user on database
                              token = parsedTokenResponse['token'];
                              register().then((value) {
                                signup_response = value;
                                Map<String, dynamic> parsedSignupResponse = jsonDecode(signup_response);
                                print(signup_response);
                                if (parsedSignupResponse["status"] == "success") {
                                  Navigator.push(context, MaterialPageRoute(builder: (_) => BusinessLoginPage()));
                                }
                              });
                            }
                          });
                        }
                        else{
                          Fluttertoast.showToast(
                              msg: "You must choose a location",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.CENTER,
                              timeInSecForIosWeb: 1,
                              backgroundColor: Colors.red,
                              textColor: Colors.white,
                              fontSize: 16.0
                          );
                        }
                      }
                    },
                    child: Text(
                      'Register',
                      style: TextStyle(color: Colors.white, fontSize: 25),
                    ),
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 15.0, right: 15.0, top: 5, bottom: 0),
                  child: new GestureDetector(
                    onTap: () {
                      Navigator.push(context, MaterialPageRoute(builder: (_) => BusinessLoginPage()));
                    },
                    child: new Text("Already have an account? Login"),
                  ),
                )
              ],
            ),
          ),
        )
    );
  }
}