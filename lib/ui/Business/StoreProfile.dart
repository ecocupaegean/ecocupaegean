import 'package:Ecocup/services/RestService.dart';
import 'package:Ecocup/services/RouteService.dart';
import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

class StoreProfile extends StatefulWidget {
  StoreProfile({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _StoreProfileState createState() => _StoreProfileState();
}

class _StoreProfileState extends State<StoreProfile> {
  int store_id = 0;
  String name = "";
  String access_token = "";

  String total_transactions = "0",
      total_value = "0",
      total_customers = "0",
      returning_customers = "0",
      loyal_customers = "0";

  String storeStatisticsResponse = "";

  @override
  void initState() {
    super.initState();
    _loadCommons();
  }

  Future<String> _loadCommons() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      store_id = prefs.getInt('store_id');
      name = prefs.getString('name');
      access_token = prefs.getString('access_token');
    });
  }

  Future<String> getStoreStatistics() async {
    Map<String, String> body = new Map();
    body.addAll({
      'store_id': store_id.toString(),
    });

    String baseUri = await RouteService().getRoute(this.context, "BaseUri");
    String storeStatisticsRoute =
        await RouteService().getRoute(this.context, "StoreStatistics");
    storeStatisticsResponse = await RestService()
        .doPostRequest(baseUri + storeStatisticsRoute, body);
    print(storeStatisticsResponse);
    try {
      Map<String, dynamic> parsedAStatisticsResponse = jsonDecode(storeStatisticsResponse);
      total_transactions = parsedAStatisticsResponse['times'].toString();
      int intTotalTrans = int.parse(total_transactions) + 1;
      total_transactions = intTotalTrans.toString();
      total_value = (parsedAStatisticsResponse['0'].toString() ?? 0);
      double intTotalVal = double.parse(total_value);
      intTotalVal = intTotalVal * 1.80;
      total_value = intTotalVal.toString();
      total_customers = (parsedAStatisticsResponse['1'].toString() ?? 0);
      returning_customers = (parsedAStatisticsResponse['2'].toString() ?? 0);
      loyal_customers = (parsedAStatisticsResponse['3'].toString() ?? 0);
    } catch (e) {
      print(e.toString());
      total_transactions = "0";
      total_value = "0";
      total_customers = "0";
      returning_customers = "0";
      loyal_customers = "0";
    }
    return storeStatisticsResponse;
  }

  Widget stats(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.25,
      child: Column(
        children: <Widget>[
          Flexible(
            child: Row(
              children: <Widget>[
                _buildStatCard('Total Transactions', total_transactions, Colors.orange),
                _buildStatCard('Total Value',total_value + " \u20ac", Colors.lightGreen),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget statsClients(BuildContext context){
    return Container(
      height: MediaQuery.of(context).size.height * 0.25,
      child: Column(
        children: [
          Flexible(
            child: Row(
              children: <Widget>[
                _buildStatCard('Total Customers', total_customers, Colors.red),
                _buildStatCard('Returning Customers', returning_customers, Colors.lightBlue),
                _buildStatCard('Loyal Customers', loyal_customers, Colors.purple),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Expanded _buildStatCard(String title, String count, MaterialColor color) {
    return Expanded(
      child: Container(
        margin: const EdgeInsets.all(8.0),
        padding: const EdgeInsets.all(10.0),
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              title,
              style: const TextStyle(
                color: Colors.white,
                fontSize: 15.0,
                fontWeight: FontWeight.w600,
              ),
            ),
            Text(
              count,
              style: const TextStyle(
                color: Colors.white,
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget homeWidget(BuildContext context) {
    return FutureBuilder(
      future: getStoreStatistics(),
      // ignore: missing_return
      builder: (context, snapshot) {
        if (snapshot.hasData ||
            snapshot.connectionState == ConnectionState.done) {
          return SingleChildScrollView(
              child: Column(
            children: [
              SizedBox(
                height: 30,
              ),
              Text(
                'Transactions Statistics',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 30,
              ),
              DefaultTabController(
                length: 3,
                child: Column(
                  children: [
                    Container(
                    child: TabBar(
                      indicatorSize: TabBarIndicatorSize.tab,
                      indicator: new BubbleTabIndicator(
                        tabBarIndicatorSize: TabBarIndicatorSize.tab,
                        indicatorHeight: 40.0,
                        indicatorColor: Colors.orange,
                      ),
                      labelStyle: new TextStyle(
                          fontSize: 16.0,
                          fontWeight: FontWeight.w600,
                          color: Colors.black),
                      labelColor: Colors.white,
                      unselectedLabelColor: Colors.black,
                      tabs: <Widget>[
                        Text('Today'),
                        Text('Last week'),
                        Text('Last Month')
                      ],
                      onTap: (index) {},
                    )),
                    Container(
                      height: MediaQuery.of(context).size.height * 0.25,
                      margin: EdgeInsets.only(top: 20),
                      child: TabBarView(
                        children: [
                          stats(context),
                          stats(context),
                          stats(context)
                        ],
                      ),
                    )
                  ],
                ),
              ),
              /* CLIENT STATISTICS */
              SizedBox(height: 20),
              Text(
                'Customers Statistics',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 20),
              DefaultTabController(
                length: 3,
                child: Column(
                  children: [
                    Container(
                        child: TabBar(
                          indicatorSize: TabBarIndicatorSize.tab,
                          indicator: new BubbleTabIndicator(
                            tabBarIndicatorSize: TabBarIndicatorSize.tab,
                            indicatorHeight: 40.0,
                            indicatorColor: Colors.orange,
                          ),
                          labelStyle: new TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.w600,
                              color: Colors.black),
                          labelColor: Colors.white,
                          unselectedLabelColor: Colors.black,
                          tabs: <Widget>[
                            Text('Today'),
                            Text('Last week'),
                            Text('Last Month')
                          ],
                          onTap: (index) {},
                        )),
                    Container(
                      height: MediaQuery.of(context).size.height * 0.25,
                      margin: EdgeInsets.only(top: 20),
                      child: TabBarView(
                        children: [
                          statsClients(context),
                          statsClients(context),
                          statsClients(context)
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ],
          ));
        } else if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(
            child: CircularProgressIndicator(
              valueColor: new AlwaysStoppedAnimation<Color>(Colors.orange),
            ),
          );
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return homeWidget(context);
  }
}
