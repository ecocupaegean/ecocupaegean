import 'package:Ecocup/ui/Business/StoreProfile.dart';
import '../Users/Login.dart';
import '../Users/Scanner.dart';
import '../Users/placeholder_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../Users/ChooseLoginPage.dart';
import 'GenerateQR.dart';
import 'Home.dart';

void main() {
  runApp(EcocupApp());
}

class EcocupApp extends StatelessWidget {
  // This widget is the root of your application.
  bool _isFirstTime = true;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Ecocup',
      theme: ThemeData(
        primarySwatch: Colors.orange,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: determineLandingScreen(context),
    );
  }

  Widget determineLandingScreen(BuildContext context) {
    return FutureBuilder(
      future: isFirstRun(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          if (_isFirstTime == true)
            return LoginPage(title: 'Ecocup');
          else
            return BusinessHomePage(title: 'Ecocup');
        } else
          return Container(); // or some other widget
      },
    );
  }

  Future<bool> isFirstRun() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _isFirstTime = (prefs.getBool('isFirstTimeBusiness') ?? true);
    return _isFirstTime;
  }
}

class BusinessHomePage extends StatefulWidget {
  BusinessHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _BusinessHomePageState createState() => _BusinessHomePageState();
}

class _BusinessHomePageState extends State<BusinessHomePage> {
  int _currentIndex = 0;
  int index = 0;

  final List<Widget> _children = [
    BusinessHome(title: "Ecocup"),
    GenerateScreen(),
    PlaceholderWidget(Colors.black),
    StoreProfile()
  ];

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
    return Scaffold(
      appBar: AppBar(
          automaticallyImplyLeading: false,
          iconTheme: IconThemeData(color: Colors.white),
          title: const Text('Ecocup', style: TextStyle(color: Colors.white)),
          backgroundColor: Colors.orange,
          actions: <Widget>[
            Padding(
                padding: EdgeInsets.only(right: 20.0),
                child: GestureDetector(
                  onTap: () {
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext context) =>
                                ChooseLoginPage()));
                  },
                  child: Icon(
                    Icons.logout,
                    color: Colors.white,
                  ),
                )),
          ]),
      body: _children[_currentIndex],
      bottomNavigationBar: new Stack(
        overflow: Overflow.visible,
        alignment: new FractionalOffset(.5, 1.0),
        children: [
          new BottomNavigationBar(
            onTap: onTabTapped,
            type: BottomNavigationBarType.fixed,
            currentIndex: _currentIndex,
            backgroundColor: Colors.white,
            selectedItemColor: Colors.orange,
            items: [
              BottomNavigationBarItem(
                icon: new Icon(Icons.home),
                title: new Text('Home'),
              ),
              BottomNavigationBarItem(
                  icon: Icon(Icons.qr_code), title: Text('Code')),
              BottomNavigationBarItem(
                  icon: new Stack(
                    children: <Widget>[
                      new Icon(Icons.notifications),
                      new Positioned(
                        right: 0,
                        child: new Container(
                          padding: EdgeInsets.all(1),
                          decoration: new BoxDecoration(
                            color: Colors.red,
                            borderRadius: BorderRadius.circular(6),
                          ),
                          constraints: BoxConstraints(
                            minWidth: 12,
                            minHeight: 12,
                          ),
                        ),
                      )
                    ],
                  ),
                  title: Text('Notifications')),
              BottomNavigationBarItem(
                icon: new Icon(Icons.person),
                title: new Text('Profile'),
              )
            ],
          ),
        ],
      ),
    );
  }
}
