import 'dart:convert';
import 'dart:ui';
import 'package:Ecocup/models/Friend.dart';
import 'package:Ecocup/services/RestService.dart';
import 'package:Ecocup/services/RouteService.dart';
import 'package:Ecocup/ui/Dialogs/SearchUsers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../main.dart';
import 'ScanSuccess.dart';

class FriendDialogBox extends StatefulWidget {
  final String title, descriptions, text;
  final Image img;

  const FriendDialogBox(
      {Key key, this.title, this.descriptions, this.text, this.img})
      : super(key: key);

  @override
  _FriendDialogBoxState createState() => _FriendDialogBoxState();
}

class _FriendDialogBoxState extends State<FriendDialogBox>
    with TickerProviderStateMixin {
  int user_id = 0;
  String baseUri = "";
  String name = "";
  String access_token = "";
  List<Friend> _users = [];
  List<Friend> _userSearchResults = [];
  List<Friend> _friends = [];
  TabController _tabController;
  bool _searching = false;

  @override
  void initState() {
    super.initState();
    _loadCommons();
    _loadFriends();
    _tabController = TabController(vsync: this, length: 2);
    _tabController.addListener(() {
      if (_tabController.indexIsChanging) {
        FocusScope.of(context).requestFocus(new FocusNode());
        if (_tabController.index == 0) {
          setState(() {
            _userSearchResults.clear();
          });
        }
      }
    });
  }

  Future<void> _loadCommons() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      user_id = prefs.getInt('user_id');
      name = prefs.getString('name');
      access_token = prefs.getString('access_token');
    });
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  Widget _buildFriendListTile(BuildContext context, int index) {
    var friend = _friends[index];

    return new ListTile(
      // onTap: () => _navigateToFriendDetails(friend, index),
      leading: new Hero(
          tag: index,
          child: new CircleAvatar(
            backgroundColor: Colors.white,
            child: ClipRect(
              child: Uri.tryParse(friend.imageUrl).hasAbsolutePath
                  ? Image.network(
                      friend.imageUrl,
                      fit: BoxFit.cover,
                    )
                  : Image.network(
                      'https://429860629.linuxzone125.grserver.gr/ecocup-api/user/images/leaf.png',
                      fit: BoxFit.cover,
                    ),
            ),
          )),
      title: new Text(
        friend.name,
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
      ),
      subtitle: new Text(friend.email),
    );
  }

  // void _navigateToFriendDetails(Friend friend, Object avatarTag) {
  //   Navigator.of(context).push(
  //     new MaterialPageRoute(
  //       builder: (c) {
  //         //return new FriendDetailsPage(friend, avatarTag: avatarTag);
  //       },
  //     ),
  //   );
  // }

  Future<List<Friend>> _loadFriends() async {
    baseUri = await RouteService().getRoute(this.context, "BaseUri");
    String friendsRoute =
        await RouteService().getRoute(this.context, "GetUserFriends");
    Map<String, String> body = new Map();
    body.addAll({
      'access_token': access_token,
      'user_id': user_id.toString(),
    });
    String friendsResponse =
        await RestService().doPostRequest(baseUri + friendsRoute, body);
    var list = json.decode(friendsResponse) as List;
    if (list != null && list.isNotEmpty)
      _friends = list.map((i) => Friend.fromJson(i)).toList();

    return Future.value(_friends);
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: contentBox(context),
    );
  }

  contentBox(context) {
    return Stack(
      children: <Widget>[
        DefaultTabController(
          length: 2,
          child: Scaffold(
            extendBodyBehindAppBar: true,
            appBar: PreferredSize(
              preferredSize: const Size.fromHeight(150),
              child: AppBar(
                automaticallyImplyLeading: false,
                elevation: 0,
                backgroundColor: Colors.transparent,
                bottom: TabBar(
                  controller: _tabController,
                  tabs: [
                    Tab(icon: Icon(Icons.person)),
                    Tab(icon: Icon(Icons.search)),
                  ],
                ),
              ),
            ),
            backgroundColor: Colors.transparent,
            body: Container(
              margin: EdgeInsets.only(top: 70),
              decoration: BoxDecoration(
                  shape: BoxShape.rectangle,
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(20),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black,
                        offset: Offset(0, 10),
                        blurRadius: 10),
                  ]),
              child: TabBarView(controller: _tabController, children: [
                Column(
                  children: [
                    Container(
                      padding: EdgeInsets.only(top: 35),
                      child: Text(
                        "Friends",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 20),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    FutureBuilder(
                      future: _loadFriends(),
                      builder: (context, snapshot) => Expanded(
                        child: snapshot.connectionState ==
                                ConnectionState.waiting
                            ? Center(
                                child: CircularProgressIndicator(),
                              )
                            : _friends.isNotEmpty
                                ? new ListView.separated(
                                    padding: EdgeInsets.only(top: 20),
                                    itemCount: _friends.length + 1,
                                    itemBuilder: (ctx, idx) {
                                      if (idx < _friends.length)
                                        return _buildFriendListTile(ctx, idx);
                                      return SizedBox(
                                        height: 20,
                                      );
                                    },
                                    separatorBuilder: (context, index) {
                                      return Divider();
                                    },
                                  )
                                : Container(
                                    padding: EdgeInsets.only(top: 80),
                                    child: Text("You have no friends"),
                                  ),
                      ),
                    )
                  ],
                ),
                Column(
                  children: [
                    Container(
                      padding: EdgeInsets.only(top: 35),
                      child: Text(
                        "Search",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 20),
                      ),
                    ),
                    SearchUsers(queryUpdateFunc: (resultsList) {
                      setState(() {
                        _userSearchResults = resultsList;
                      });
                    }, progressUpdateFunc: (searching) {
                      setState(() {
                        _searching = searching;
                      });
                    }),
                    Expanded(
                      child: searchResults,
                    )
                  ],
                ),
              ]),
            ),
          ),
        ),
        Align(
            alignment: Alignment.bottomCenter,
            child: ElevatedButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text(
                'Done',
                style: TextStyle(color: Colors.white, fontSize: 25),
              ),
              style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.blue),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: BorderSide(color: Colors.blue)))),
            )),
        Positioned(
          left: 20,
          right: 20,
          child: CircleAvatar(
            backgroundColor: Colors.white,
            radius: 45,
            child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(45)),
                child: Image.asset("assets/images/friends.png")),
          ),
        ),
      ],
    );
  }

  bool _isFriend(Friend friend) {
    for (int i = 0; i < _friends.length; i++) {
      if (_friends[i].id == friend.id) return true;
    }
    return false;
  }

  Widget get searchResults {
    if (_searching) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }
    if (_userSearchResults.isNotEmpty) {
      return ListView.separated(
        padding: EdgeInsets.only(top: 0),
        itemCount: _userSearchResults.length + 1,
        itemBuilder: (ctx, idx) {
          if (idx < _userSearchResults.length)
            return _buildUsersListTile(ctx, idx);
          return SizedBox(
            height: 20,
          );
        },
        separatorBuilder: (context, index) {
          return Divider();
        },
      );
    }
    return Container();
  }

  Widget _buildUsersListTile(BuildContext context, int index) {
    if (_userSearchResults.isEmpty) return null;
    var friend = _userSearchResults[index];
    return new ListTile(
      // onTap: () => _navigateToFriendDetails(friend, index),
      leading: new Hero(
          tag: index,
          child: new CircleAvatar(
            backgroundColor: Colors.white,
            child: ClipRect(
              child: Image.network(
                'https://429860629.linuxzone125.grserver.gr/ecocup-api/user/images/leaf.png',
                fit: BoxFit.cover,
              ),
            ),
          )),
      title: new Text(friend.name),
      subtitle: new Text(friend.email),
      trailing: _isFriend(friend)
          ? IconButton(
              onPressed: () {},
              icon: Icon(
                Icons.account_box,
                color: Colors.blue,
              ))
          : (friend.id == user_id.toString()
              ? null
              : IconButton(
                  onPressed: () {
                    _addFriend(friend.id);
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return ScanSuccessDialogBox(
                            title: "Friend request sent",
                            descriptions:
                                "The user will get notified for your friend request",
                            text: "Done",
                          );
                        });
                  },
                  icon: new Icon(Icons.add_circle_outline),
                  color: Colors.lightBlueAccent,
                  iconSize: 30.0,
                )),
    );
  }

  Future<String> _addFriend(String asked_user_id) async {
    baseUri = await RouteService().getRoute(this.context, "BaseUri");
    String addFriendRoute =
        await RouteService().getRoute(this.context, "AddFriend");
    Map<String, String> body = new Map();
    body.addAll({
      'access_token': access_token,
      'user_id': user_id.toString(),
      'asked_user_id': asked_user_id,
      'name': name
    });
    String addFriendResponse =
        await RestService().doPostRequest(baseUri + addFriendRoute, body);
    return addFriendResponse;
  }
}
