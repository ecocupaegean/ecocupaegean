import 'dart:convert';
import 'dart:ui';
import 'package:Ecocup/main.dart';
import 'package:Ecocup/models/NotificationDto.dart';
import 'package:Ecocup/services/RestService.dart';
import 'package:Ecocup/services/RouteService.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NotificationDialogBox extends StatefulWidget {
  final String title, descriptions, text;
  final Image img;
  final NotificationDto notificationDto;

  const NotificationDialogBox({Key key, this.title, this.descriptions, this.text, this.img, this.notificationDto}) : super(key: key);

  @override
  _NotificationDialogBoxState createState() => _NotificationDialogBoxState(notificationDto);
}

class _NotificationDialogBoxState extends State<NotificationDialogBox> {

  int user_id = 0;
  String baseUri = "";
  String name = "";
  String access_token = "";
  NotificationDto notificationDto;

  _NotificationDialogBoxState(NotificationDto notificationDto){
    this.notificationDto = notificationDto;
  }

  @override
  void initState(){
    super.initState();
    _loadCommons();
  }

  Future<void> _loadCommons() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      user_id = prefs.getInt('user_id');
      name = prefs.getString('name');
      access_token = prefs.getString('access_token');
    });
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: contentBox(context),
    );
  }

  Future<void> _acceptFriend(String asked_user_id) async{
    baseUri = await RouteService().getRoute(this.context,"BaseUri");
    String acceptFriendRoute = await RouteService().getRoute(this.context,"AcceptFriendRequest");
    Map<String,String> body = new Map();
    body.addAll({
      'access_token': access_token,
      'user_id': user_id.toString(),
      'asked_user_id': asked_user_id
    });
    print(user_id.toString() + " " + asked_user_id);
    String acceptFriendResponse = await RestService().doPostRequest(baseUri+acceptFriendRoute, body);
    _notificationsSeen();
  }

  Future<void> _notificationsSeen() async{
    baseUri = await RouteService().getRoute(this.context,"BaseUri");
    String notificationsSeenRoute = await RouteService().getRoute(this.context,"NotificationsSeen");
    Map<String,String> body = new Map();
    body.addAll({
      'access_token': access_token,
      'user_id': user_id.toString(),
    });
    String notificationsSeenResponse = await RestService().doPostRequest(baseUri+notificationsSeenRoute, body);
  }

  contentBox(context){
    return Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(left: 20,top: 65, right: 20,bottom: 20
          ),
          margin: EdgeInsets.only(top: 45),
          decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              boxShadow: [
                BoxShadow(color: Colors.black,offset: Offset(0,10),
                    blurRadius: 10
                ),
              ]
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(widget.title,style: TextStyle(fontSize: 22,fontWeight: FontWeight.w600),),
              SizedBox(height: 15,),
              Text(widget.descriptions,style: TextStyle(fontSize: 17),textAlign: TextAlign.center,),
              SizedBox(height: 22,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Align(
                      alignment: Alignment.bottomLeft,
                      child: ElevatedButton(
                        onPressed: (){
                          _acceptFriend(notificationDto.description.split("---")[1]);
                          Navigator.push(context, MaterialPageRoute(builder: (_) => EcocupApp()));
                        },
                        child: Text(
                          'Accept',
                          style: TextStyle(color: Colors.blue, fontSize: 25),
                        ),
                        style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.white),shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18.0),
                                side: BorderSide(color: Colors.blue)
                            )
                        )),
                      )
                  ),
                  Align(
                      alignment: Alignment.bottomRight,
                      child: ElevatedButton(
                        onPressed: (){
                          Navigator.push(context, MaterialPageRoute(builder: (_) => EcocupApp()));
                        },
                        child: Text(
                          'Decline',
                          style: TextStyle(color: Colors.blue, fontSize: 25),
                        ),
                        style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.white),shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18.0),
                                side: BorderSide(color: Colors.blue)
                            )
                        )),
                      )
                  ),
                ],
              )
            ],
          ),
        ),
        Positioned(
          left: 20,
          right: 20,
          child: CircleAvatar(
            backgroundColor: Colors.transparent,
            radius: 45,
            child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(45)),
                child: Image.asset("assets/images/tree-pl.png")
            ),
          ),
        ),
      ],
    );
  }
}