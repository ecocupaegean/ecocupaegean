import 'dart:convert';

import 'package:Ecocup/models/Friend.dart';
import 'package:Ecocup/services/RestService.dart';
import 'package:Ecocup/services/RouteService.dart';
import 'package:Ecocup/ui/Dialogs/ScanSuccess.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SearchUsers extends StatefulWidget {
  final Function(List) queryUpdateFunc;
  final Function(bool) progressUpdateFunc;

  SearchUsers(
      {@required this.queryUpdateFunc, @required this.progressUpdateFunc});
  @override
  _SearchUsersState createState() => _SearchUsersState();
}

class _SearchUsersState extends State<SearchUsers> {
  _SearchUsersState();

  int user_id = 0;
  String baseUri = "";
  String name = "";
  String access_token = "";
  List<Friend> _userSearchQuery = [];
  List<Friend> _friends = [];
  List<Friend> _users = [];
  final _searchFormKey = GlobalKey<FormState>();
  FocusNode _searchFocusNode = FocusNode();
  TextEditingController _searchController = TextEditingController();
  bool _searching = false;

  @override
  void dispose() {
    _searchFocusNode.dispose();
    _searchController.dispose();
    super.dispose();
  }

  void initState() {
    super.initState();
    _loadCommons();
  }

  Future<void> _loadCommons() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      user_id = prefs.getInt('user_id');
      name = prefs.getString('name');
      access_token = prefs.getString('access_token');
    });
  }

  Future<void> _searchUsers(String searchQuery) async {
    try {
      baseUri = await RouteService().getRoute(this.context, "BaseUri");
      String searchUsersRoute =
          await RouteService().getRoute(this.context, "GetUserSearch");
      Map<String, String> body = new Map();
      body.addAll({
        'access_token': access_token,
        'query': searchQuery,
      });
      String filteredUsersResponse =
          await RestService().doPostRequest(baseUri + searchUsersRoute, body);
      var list = json.decode(filteredUsersResponse) as List;
      if (list != null && list.isNotEmpty)
        _userSearchQuery = list.map((i) => Friend.fromJson(i)).toList();

      widget.queryUpdateFunc(_userSearchQuery);
      return;
    } catch (e) {
      _userSearchQuery = await _loadAllUsers();
      widget.queryUpdateFunc(_userSearchQuery);
      return;
    }
  }

  Future<List<Friend>> _loadAllUsers() async {
    baseUri = await RouteService().getRoute(this.context, "BaseUri");
    String allUsersRoute =
        await RouteService().getRoute(this.context, "GetAllUsers");
    Map<String, String> body = new Map();
    body.addAll({
      'access_token': access_token,
      'user_id': user_id.toString(),
    });

    String allUsersResponse =
        await RestService().doPostRequest(baseUri + allUsersRoute, body);
    var list = json.decode(allUsersResponse) as List;
    _users = list.map((i) => Friend.fromJson(i)).toList();
    return Future.value(_users);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(top: 40, bottom: 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: _form,
          ),
        ],
      ),
    );
  }

  Form get _form {
    return Form(
        key: _searchFormKey,
        child: TextFormField(
          textInputAction: TextInputAction.search,
          onFieldSubmitted: (value) {
            _searchFocusNode.unfocus();
            bool isValid = _searchFormKey.currentState.validate();
            if (isValid) {
              _searchFormKey.currentState.save();
            }
          },
          onSaved: (value) async {
            _searching = true;
            widget.progressUpdateFunc(_searching);
            await _searchUsers(_searchController.text);
            _searching = false;
            widget.progressUpdateFunc(_searching);
          },
          controller: _searchController,
          autocorrect: false,
          focusNode: _searchFocusNode,
          keyboardType: TextInputType.text,
          decoration: InputDecoration(
            contentPadding:
                const EdgeInsets.symmetric(vertical: 5.0, horizontal: 10.0),
            isDense: false,
            suffixIcon: IconButton(
                onPressed: () async {
                  _searchFocusNode.unfocus();
                  bool isValid = _searchFormKey.currentState.validate();
                  if (isValid) {
                    _searching = true;
                    widget.progressUpdateFunc(_searching);
                    _searchFormKey.currentState.save();
                    await _searchUsers(_searchController.text);
                    _searching = false;
                    widget.progressUpdateFunc(_searching);
                  }
                },
                icon: Icon(Icons.search)),
            labelText: "Find friends",
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(25.0),
              borderSide: BorderSide(
                color: Colors.blue,
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(25.0),
              borderSide: BorderSide(
                color: Colors.grey,
                width: 1.0,
              ),
            ),
          ),
        ));
  }
}
