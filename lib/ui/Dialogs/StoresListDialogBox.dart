import 'dart:convert';
import 'dart:ui';
import 'package:Ecocup/models/Friend.dart';
import 'package:Ecocup/models/VisitedStore.dart';
import 'package:Ecocup/services/RestService.dart';
import 'package:Ecocup/services/RouteService.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class StoresListDialogBox extends StatefulWidget {
  final String title, descriptions, text;
  final Image img;

  StoresListDialogBox(
      {Key key, this.title, this.descriptions, this.text, this.img})
      : super(key: key);

  @override
  _StoresListDialogBoxState createState() => _StoresListDialogBoxState();
}

class _StoresListDialogBoxState extends State<StoresListDialogBox> {
  int user_id = 0;
  String baseUri = "";
  String name = "";
  String access_token = "";
  String storesResponse = "";
  List<VisitedStore> _visitedStores = [];

  @override
  void initState() {
    super.initState();
    //_loadCommons();
  }

  Future<void> _loadCommons() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    user_id = prefs.getInt('user_id');
    name = prefs.getString('name');
    access_token = prefs.getString('access_token');
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: contentBox(context),
    );
  }

  Widget _buildStoresListTile(BuildContext context, int index) {
    var store = _visitedStores[index];
    return new ListTile(
        leading: new Hero(
          tag: index,
          child: new CircleAvatar(
            backgroundImage: new NetworkImage(
                "https://429860629.linuxzone125.grserver.gr/ecocup-api/user/images/ecocup.png"),
          ),
        ),
        title: new Text(
          store.name,
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
        ),
        subtitle: new Text("Last visited on: ") //+ store.lastSeen),
        );
  }

  Future<String> getUserVisitedStores() async {
    await _loadCommons();
    Map<String, String> body = new Map();
    body.addAll({
      'access_token': access_token,
      'user_id': user_id.toString(),
    });
    baseUri = await RouteService().getRoute(this.context, "BaseUri");
    String allStoresRoute =
        await RouteService().getRoute(this.context, "GetAllStores");
    storesResponse =
        await RestService().doPostRequest(baseUri + allStoresRoute, body);
    var list = json.decode(storesResponse) as List;
    _visitedStores = list.map((i) => VisitedStore.fromJson(i)).toList();
    return Future.value(storesResponse);
  }

  contentBox(context) {
    return FutureBuilder(
        future: getUserVisitedStores(),
        builder: (context, snapshot) {
          if (snapshot.hasData ||
              snapshot.connectionState == ConnectionState.done) {
            return Stack(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: 70),
                  decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black,
                            offset: Offset(0, 10),
                            blurRadius: 10),
                      ]),
                  child: Column(
                    children: [
                      Expanded(
                          child: snapshot.connectionState ==
                                  ConnectionState.waiting
                              ? Center(
                                  child: CircularProgressIndicator(),
                                )
                              : Container(
                                  margin: new EdgeInsets.only(top: 10.00),
                                  child: new ListView.separated(
                                    itemCount: _visitedStores.length,
                                    itemBuilder: _buildStoresListTile,
                                    separatorBuilder:
                                        (BuildContext context, int index) {
                                      return Divider();
                                    },
                                  ),
                                )),
                    ],
                  ),
                ),
                Align(
                    alignment: Alignment.bottomCenter,
                    child: ElevatedButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        'Done',
                        style: TextStyle(color: Colors.white, fontSize: 25),
                      ),
                      style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all(Colors.blue),
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(18.0),
                                      side: BorderSide(color: Colors.blue)))),
                    )),
                Positioned(
                  left: 20,
                  right: 20,
                  child: CircleAvatar(
                    backgroundColor: Colors.white,
                    radius: 45,
                    child: ClipRRect(
                        borderRadius: BorderRadius.all(Radius.circular(45)),
                        child: Image.asset(
                            "assets/images/ecocup_placeholder_blue.png")),
                  ),
                ),
              ],
            );
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        });
  }
}
