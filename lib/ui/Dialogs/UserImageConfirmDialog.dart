import 'package:flutter/material.dart';
import 'package:image_pickers/image_pickers.dart';
import 'dart:io';

AlertDialog imageConfirmationDialog(
    BuildContext context, File image, Function setImage) {
  return AlertDialog(
    title: Center(child: Text("Preview profile picture")),
    content: Container(
      child: FittedBox(
        fit: BoxFit.scaleDown,
        child: CircleAvatar(
          radius: 60,
          backgroundImage: FileImage(image),
        ),
      ),
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        border: Border.all(width: 1),
      ),
    ),
    actions: [
      TextButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: Text("Cancel", style: TextStyle(color: Colors.red))),
      TextButton(
          onPressed: () {
            setImage(image);
            Navigator.of(context).pop();
          },
          child: Text("Confirm")),
    ],
  );
}
