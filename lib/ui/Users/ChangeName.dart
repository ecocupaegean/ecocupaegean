import 'dart:convert';

import 'package:Ecocup/services/RestService.dart';
import 'package:Ecocup/services/RouteService.dart';
import 'package:Ecocup/ui/Dialogs/ReplaceCodeDialog.dart';
import 'package:Ecocup/ui/Dialogs/UserInfoUpdateDialogs.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'ChooseLoginPage.dart';

class ChangeNameScreen extends StatefulWidget {
  @override
  _ChangeNameScreenState createState() => _ChangeNameScreenState();
}

class _ChangeNameScreenState extends State<ChangeNameScreen> {
  int user_id = 0;
  String name = "";
  String last_name = "";
  String access_token = "";
  final _formKey = GlobalKey<FormState>();
  final firstNameFocusNode = FocusNode();
  final lastNameFocusNode = FocusNode();
  final fname_controller = TextEditingController();
  final lname_controller = TextEditingController();
  bool _isloading = true;

  Future<void> _loadCommons() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      user_id = prefs.getInt('user_id');
      name = prefs.getString('name');
      access_token = prefs.getString('access_token');
      fname_controller.text = name;
      lname_controller.text = last_name;
    });
  }

  Future<void> _saveData(String firstName, String lastName) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('name', firstName);
    prefs.setString('last_name', lastName);
  }

  @override
  void initState() {
    super.initState();
    _loadCommons().then((value) {
      setState(() {
        _isloading = false;
      });
    });
  }

  Future<void> _submit() async {
    if (!_formKey.currentState.validate()) {
      return;
    }
    _formKey.currentState.save();
    setState(() {
      _isloading = true;
    });
    String fname = fname_controller.text;
    String lname = lname_controller.text;
    Map<String, String> body = new Map();
    body.addAll(
        {'user_id': user_id.toString(), 'name': fname, 'last_name': lname});

    String baseUri = await RouteService().getRoute(this.context, "BaseUri");
    String passwordUpdateRoute =
        await RouteService().getRoute(this.context, "UserNameUpdateRoute");
    var responseString =
        await RestService().doPostRequest(baseUri + passwordUpdateRoute, body);
    setState(() {
      _isloading = false;
    });
    if (responseString.length > 0) {
      Map<String, dynamic> parsedResponse = jsonDecode(responseString);
      if (parsedResponse['status'] == "success") {
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return successfulUpdateDialog(context, "Name");
            }).then((value) {
          _saveData(fname, lname);
          Navigator.of(context).pop();
        });
      } else if (parsedResponse['status'] == "failed") {
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return failedUpdateDialog(context, "name");
            });
      }
    } else {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return failedUpdateDialog(context, "name");
          });
    }
  }

  @override
  void dispose() {
    fname_controller.dispose();
    lname_controller.dispose();
    firstNameFocusNode.dispose();
    lastNameFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.white),
        title: const Text('Ecocup', style: TextStyle(color: Colors.white)),
        backgroundColor: Colors.blue,
        actions: <Widget>[
          Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () {
                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return ReplaceCodeDialog(
                          title: "Earn Rewards",
                          descriptions:
                              "Visit RePlace | Resolution app to play and earn a code for your rewards",
                          text: "Done",
                        );
                      });
                },
                child: Icon(
                  Icons.code,
                  color: Colors.white,
                ),
              )),
          Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () {
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) =>
                              ChooseLoginPage()));
                },
                child: Icon(
                  Icons.logout,
                  color: Colors.white,
                ),
              )),
        ],
      ),
      body: _isloading
          ? Center(child: CircularProgressIndicator())
          : GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: () => FocusScope.of(context).unfocus(),
              child: Container(
                constraints: BoxConstraints.expand(),
                child: SingleChildScrollView(
                    padding: EdgeInsets.only(top: 20, left: 20, right: 20),
                    child: Container(
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text("Change Name",
                                  style: TextStyle(
                                      fontSize: 25,
                                      fontWeight: FontWeight.bold)),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 20),
                            child: Column(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Form(
                                    key: _formKey,
                                    child: Column(
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: TextFormField(
                                            controller: fname_controller,
                                            focusNode: firstNameFocusNode,
                                            autocorrect: false,
                                            decoration: InputDecoration(
                                              labelText: "First Name",
                                              focusedBorder: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(25.0),
                                                borderSide: BorderSide(
                                                  color: Colors.blue,
                                                ),
                                              ),
                                              enabledBorder: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(25.0),
                                                borderSide: BorderSide(
                                                  color: Colors.grey,
                                                  width: 1.0,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: TextFormField(
                                            controller: lname_controller,
                                            focusNode: lastNameFocusNode,
                                            autocorrect: false,
                                            decoration: InputDecoration(
                                              labelText: "Last Name",
                                              focusedBorder: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(25.0),
                                                borderSide: BorderSide(
                                                  color: Colors.blue,
                                                ),
                                              ),
                                              enabledBorder: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(25.0),
                                                borderSide: BorderSide(
                                                  color: Colors.grey,
                                                  width: 1.0,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 20.0),
                            child: TextButton(
                                style: TextButton.styleFrom(
                                    primary: Colors.white,
                                    backgroundColor: Colors.blue),
                                onPressed: () {
                                  FocusScope.of(context).unfocus();
                                  _submit();
                                },
                                child: Text("Save")),
                          ),
                        ],
                      ),
                    )),
              ),
            ),
    );
  }
}
