import 'dart:async';
import 'dart:convert';
import 'package:Ecocup/main.dart';
import 'package:Ecocup/services/RestService.dart';
import 'package:Ecocup/services/RouteService.dart';
import 'package:Ecocup/services/ValidatorService.dart';
import 'package:Ecocup/ui/Business/main.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'Signup.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _formKey = GlobalKey<FormState>();

  String email = "";
  String password = "";

  String credentialsResponse = "";
  String authResponse = "";

  String name = "";
  String access_token = "";
  String user_id = "";

  bool isLoading = false;

  Future<String> credentials() async{
    Map<String,String> body = new Map();
    body.addAll({
      'email':email,
      'password':password
    });

    String baseUri = await RouteService().getRoute(this.context,"BaseUri");
    String userInfoRoute = await RouteService().getRoute(this.context,"UserInfoRoute");
    credentialsResponse = await RestService().doPostRequest(baseUri+userInfoRoute, body);
    return credentialsResponse;
  }

  Future<String> authenticate(String user_credentials) async{
    Map<String,String> body = new Map();
    Map<String,dynamic> parsedACredentialsResponse;
    print("user credentials " + user_credentials);
    try{
      if(!user_credentials.contains("unknown")) {
        parsedACredentialsResponse = jsonDecode(user_credentials);
        body.addAll({
          'access_token': parsedACredentialsResponse['code'],
          'user_id': parsedACredentialsResponse['id'],
          'email': parsedACredentialsResponse['email'],
          'password': password
        });
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.clear();

        prefs.setInt('user_id', int.parse(parsedACredentialsResponse['id']));
        prefs.setString('name', parsedACredentialsResponse['name']);
        prefs.setString('access_token', parsedACredentialsResponse['code']);
        prefs.setBool('isFirstTime', false);

        String baseUri = await RouteService().getRoute(this.context,"BaseUri");
        String loginRoute = await RouteService().getRoute(this.context,"LoginRoute");
        authResponse = await RestService().doPostRequest(baseUri+loginRoute, body);
      }
      else if(user_credentials.contains("unknown")){
        //wrong password
        String baseUri = await RouteService().getRoute(this.context,"BaseUri");
        String loginRoute = await RouteService().getRoute(this.context,"LoginRoute");
        authResponse = await RestService().doPostRequest(baseUri+loginRoute, body);
      }
    }
    catch(e){}
    return authResponse;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: const Text('Ecocup', style: TextStyle(
            color: Colors.black
        )),
        backgroundColor: Colors.white,
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child:Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 60.0),
                child: Center(
                  child: Container(
                      width: 200,
                      height: 150,
                      child: Image.asset('assets/images/ecocup.png')),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left:15.0,right: 15.0,top:50,bottom: 0),
                child: TextFormField(
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      enabledBorder: new OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(25.0),
                        borderSide:  BorderSide(color: Colors.lightBlue ),
                      ),
                      focusedBorder: new OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(25.0),
                        borderSide:  BorderSide(color: Colors.lightBlue ),
                      ),
                      labelText: 'Email',
                      hintText: 'yourname@gmail.com'),
                  validator: (value){
                    bool isValid = ValidatorService().validateEmail(value);
                    if(!isValid) return 'Please enter a valid email';
                    else email = value;
                    return null;
                  }
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15.0, right: 15.0, top: 15, bottom: 0),
                child: TextFormField(
                  obscureText: true,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      enabledBorder: new OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(25.0),
                        borderSide:  BorderSide(color: Colors.lightBlue ),
                      ),
                      focusedBorder: new OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(25.0),
                        borderSide:  BorderSide(color: Colors.lightBlue ),
                      ),
                      labelText: 'Password',
                      hintText: 'Enter your password'),
                    validator: (value) {
                      //bool isValid = ValidatorService().validatePassword(value);
                      bool isValid = true;
                      if (!isValid) {
                        return 'Password must be at least 6 characters long';
                      }
                      else{
                        password = value;
                      }
                      return null;
                    }
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15.0, right: 15.0, top: 145, bottom: 0),
                child: FlatButton(
                  onPressed: (){
                    //TODO FORGOT PASSWORD
                  },
                  child: Text(
                    'Forgot Password',
                    style: TextStyle(color: Colors.blue, fontSize: 15),
                  ),
                )
              ),
              isLoading ? CircularProgressIndicator():Container(
                height: 50,
                width: 250,
                decoration: BoxDecoration(
                    color: Colors.blue, borderRadius: BorderRadius.circular(20)),
                child: FlatButton(
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      setState(() {
                        isLoading = true;
                      });
                      try{
                        credentials().then((value) {
                          credentialsResponse = value;
                          authenticate(credentialsResponse).then((value) {
                            authResponse = value;
                            if (authResponse != "" && authResponse != " " && authResponse != null) {
                              Map<String, dynamic> parsedAuthResponse = jsonDecode(authResponse);

                              if (parsedAuthResponse['status'] == "success") {
                                Navigator.pushReplacement(context,
                                    MaterialPageRoute(
                                        builder: (_) => HomePage()));
                              }
                              else if (parsedAuthResponse['status'] == "failed"){
                                Fluttertoast.showToast(
                                    msg: "Invalid username or password",
                                    toastLength: Toast.LENGTH_SHORT,
                                    gravity: ToastGravity.CENTER,
                                    timeInSecForIosWeb: 1,
                                    backgroundColor: Colors.pink,
                                    textColor: Colors.white,
                                    fontSize: 16.0
                                );
                              }
                              else{
                                Fluttertoast.showToast(
                                    msg: "User doesn't exist. Please sign up.",
                                    toastLength: Toast.LENGTH_SHORT,
                                    gravity: ToastGravity.CENTER,
                                    timeInSecForIosWeb: 1,
                                    backgroundColor: Colors.pink,
                                    textColor: Colors.white,
                                    fontSize: 20.0
                                );
                              }
                            }
                          });
                        });
                      }
                      catch (Exception ){
                        setState(() {
                          isLoading = false;
                        });
                      }
                      setState(() {
                        isLoading = false;
                      });
                    }
                  },
                  child: Text(
                    'Login',
                    style: TextStyle(color: Colors.white, fontSize: 25),
                  ),
                ),
              ),
              Padding(
                  padding: const EdgeInsets.only(left: 15.0, right: 15.0, top: 15, bottom: 0),
                child: new GestureDetector(
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(builder: (_) => SignUpPage(title: 'Ecocup')));
                    },
                  child: new Text("Don't have an account? Register"),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}