
import 'package:Ecocup/models/Globals.dart';
import 'package:Ecocup/services/RestService.dart';
import 'package:Ecocup/services/RouteService.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
import 'package:Ecocup/models/NotificationDto.dart';

import '../../main.dart';
import '../Dialogs/NotificationDialogBox.dart';


class Notifications extends StatefulWidget{
  Notifications({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _NotificationsState createState() => _NotificationsState();
}

class _NotificationsState extends State<Notifications> {
  int user_id = 0;
  String baseUri = "";
  String name = "";
  String access_token = "";
  List<NotificationDto> _notifications = [];

  @override
  void initState(){
    super.initState();
    _loadCommons();
    showBadge = false;
  }

  Future<void> _loadCommons() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      user_id = prefs.getInt('user_id');
      name = prefs.getString('name');
      access_token = prefs.getString('access_token');
    });
  }

  Future<List<NotificationDto>> _loadNotifications() async {
    baseUri = await RouteService().getRoute(this.context,"BaseUri");
    String notificationsRoute = await RouteService().getRoute(this.context,"GetUserNotifications");
    Map<String,String> body = new Map();
    body.addAll({
      'access_token': access_token,
      'user_id': user_id.toString(),
    });
    String notificationsResponse = await RestService().doPostRequest(baseUri+notificationsRoute, body);
    var list = json.decode(notificationsResponse) as List;
    _notifications = list.map((i)=>NotificationDto.fromJson(i)).toList();

    return Future.value(_notifications);
  }

  Widget notificationsWidget(BuildContext context) {
    return FutureBuilder(
      future: _loadNotifications(),
      // ignore: missing_return
      builder: (context, snapshot) {
        if (snapshot.hasData ||
            snapshot.connectionState == ConnectionState.done) {
          return SafeArea(
              child: SingleChildScrollView(
            child: Column(children: [
              SizedBox(
                height: 20,
              ),
              Text(
                'Notifications',
                style: TextStyle(color: Colors.black, fontSize: 25,fontWeight: FontWeight.bold),
              ),
              Container(
                child: new ListView.builder(
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  itemCount: _notifications.length,
                  itemBuilder: _buildNotificationsListTile,

                ),
              ),
            ]),
          ));
        } else if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }

  Widget _buildNotificationsListTile(BuildContext context, int index) {
    var notification = _notifications[index];
    return new ListTile(
      onTap: () => _notifAction(notification, index),
      leading: new Hero(
          tag: index,
          child: new CircleAvatar(
            backgroundColor: Colors.white,
            child: ClipRect(
              child: Image.network(
                'https://429860629.linuxzone125.grserver.gr/ecocup-api/user/images/notification.png',
                fit: BoxFit.cover,
              ),
            ),
          )),
      title: notification.seen == "false" ? new Text(notification.title, style: TextStyle(fontWeight: FontWeight.bold)) : new Text(notification.title),
      subtitle: notification.seen == "false" ? new Text("New friend request from " + notification.description.split("---")[0], style: TextStyle(fontWeight: FontWeight.bold)) : new Text("New friend request from " + notification.description.split("---")[0]),
    );
  }

  void _notifAction(NotificationDto notificationDto, int index){
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return NotificationDialogBox(
            title: "Friend Request",
            descriptions: notificationDto.description.split("---")[0] + " has sent you a friend request",
            text: "Done",
            notificationDto: notificationDto,
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return notificationsWidget(context);
  }
}