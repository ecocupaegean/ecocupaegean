import 'package:Ecocup/ui/Dialogs/ReplaceCodeDialog.dart';
import 'package:Ecocup/ui/Users/ChangeName.dart';
import 'package:Ecocup/ui/Users/ChangePassword.dart';
import 'package:Ecocup/ui/Users/ChooseLoginPage.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingsScreen extends StatefulWidget {
  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  int user_id = 0;
  String name = "";
  String access_token = "";
  String email = "";
  bool isloading = true;

  Future<void> _loadCommons() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    user_id = prefs.getInt('user_id');
    name = prefs.getString('name');
    access_token = prefs.getString('access_token');
    email = prefs.getString("email");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white),
          title: const Text('Ecocup', style: TextStyle(color: Colors.white)),
          backgroundColor: Colors.blue,
          actions: <Widget>[
            Padding(
                padding: EdgeInsets.only(right: 20.0),
                child: GestureDetector(
                  onTap: () {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return ReplaceCodeDialog(
                            title: "Earn Rewards",
                            descriptions:
                                "Visit RePlace | Resolution app to play and earn a code for your rewards",
                            text: "Done",
                          );
                        });
                  },
                  child: Icon(
                    Icons.code,
                    color: Colors.white,
                  ),
                )),
            Padding(
                padding: EdgeInsets.only(right: 20.0),
                child: GestureDetector(
                  onTap: () {
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext context) =>
                                ChooseLoginPage()));
                  },
                  child: Icon(
                    Icons.logout,
                    color: Colors.white,
                  ),
                )),
          ],
        ),
        body: FutureBuilder(
          future: _loadCommons(),
          builder: (context, snapshot) => snapshot.connectionState ==
                  ConnectionState.waiting
              ? Center(
                  child: CircularProgressIndicator(),
                )
              : Container(
                  child: SingleChildScrollView(
                  padding: EdgeInsets.only(top: 20),
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(bottom: 10),
                        child: ListTile(
                          leading: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                "Settings",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 25),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 25, right: 25),
                        child: InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        ChangeNameScreen()));
                          },
                          child: Container(
                            decoration: BoxDecoration(
                                border: Border(
                                    bottom: BorderSide(
                                        width: 0.5, color: Colors.grey))),
                            child: ListTile(
                              leading: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text("Name"),
                                ],
                              ),
                              title: Container(
                                alignment: Alignment.centerRight,
                                child: Text(
                                  name,
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ),
                              trailing: Container(
                                child: Icon(Icons.arrow_forward),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 25, right: 25),
                        child: Container(
                          decoration: BoxDecoration(
                              border: Border(
                                  bottom: BorderSide(
                                      width: 0.5, color: Colors.grey))),
                          child: ListTile(
                            leading: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text("E-mail"),
                              ],
                            ),
                            title: Container(
                              alignment: Alignment.centerRight,
                              child: Text(
                                email,
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            ),
                            trailing: Container(
                              child: Icon(Icons.lock),
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 25, right: 25),
                        child: InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        ChangePasswordScreen()));
                          },
                          child: Container(
                            child: ListTile(
                              leading: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text("Change Password"),
                                ],
                              ),
                              trailing: Container(
                                child: Icon(Icons.arrow_forward),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                )),
        ));
  }
}
