import 'dart:math';
import 'dart:convert';
import 'package:Ecocup/services/RestService.dart';
import 'package:Ecocup/services/RouteService.dart';
import 'package:Ecocup/services/ValidatorService.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'Login.dart';

class SignUpPage extends StatefulWidget {
  SignUpPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  final _formKey = GlobalKey<FormState>();

  int user_id = -1;
  String token_response = " ";
  String signup_response = " ";
  String token = " ";
  String email = "";
  String password = "";
  String name = "";
  String country = "";
  String phone = "";

  Future<String> generateToken() async{
    Random random = new Random();
    user_id = random.nextInt(15000000);
    Map<String,String> body = new Map();
    body.addAll({
      "user_id": user_id.toString()
    });
    String base_uri_route = await RouteService().getRoute(this.context,"BaseUri");
    String generate_token_route = await RouteService().getRoute(this.context,"GenerateToken");
    String api_token_response = await RestService().doPostRequest(base_uri_route + generate_token_route, body);

    return api_token_response;
  }

  Future<String> register() async{
    Map<String,String> body = new Map();
    print(email + name + country + phone + password);
    body.addAll({
      "name":name,
      "country":country,
      "phone":phone,
      "email":email,
      "password":password,
      "user_id": user_id.toString()
    });

    String base_uri_route = await RouteService().getRoute(this.context,"BaseUri");
    String signup_route = await RouteService().getRoute(this.context,"SignupRoute");
    String api_signup_response = await RestService().doPostRequest(base_uri_route + signup_route, body);

    return api_signup_response;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(
            color: Colors.white
        ),
        title: const Text('Ecocup', style: TextStyle(
            color: Colors.black
        )),
        backgroundColor: Colors.white,
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 60.0),
                child: Center(
                  child: Container(
                      width: 200,
                      height: 150,
                      child: Image.asset('assets/images/ecocup.png')),
                ),
              ),
              Padding(
                //padding: const EdgeInsets.only(left:15.0,right: 15.0,top:0,bottom: 0),
                padding: EdgeInsets.only(left:15.0,right: 15.0,top:50,bottom: 0),
                child: TextFormField(
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      enabledBorder: new OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(25.0),
                        borderSide:  BorderSide(color: Colors.lightBlue ),
                      ),
                      focusedBorder: new OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(25.0),
                        borderSide:  BorderSide(color: Colors.lightBlue ),
                      ),
                      labelText: 'Email',
                      hintText: 'yourname@gmail.com'),
                    validator: (value){
                      bool isValid = ValidatorService().validateEmail(value);
                      //if(!isValid) return 'Please enter a valid email';
                      //else email = value;
                      email = value;
                      return null;
                    }
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15.0, right: 15.0, top: 15, bottom: 0),
                //padding: EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  obscureText: true,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      enabledBorder: new OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(25.0),
                        borderSide:  BorderSide(color: Colors.lightBlue ),
                      ),
                      focusedBorder: new OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(25.0),
                        borderSide:  BorderSide(color: Colors.lightBlue ),
                      ),
                      labelText: 'Password',
                      hintText: 'Enter your password'),
                  validator: (value){
                    password = value;
                    return null;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15.0, right: 15.0, top: 15, bottom: 0),
                //padding: EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      enabledBorder: new OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(25.0),
                        borderSide:  BorderSide(color: Colors.lightBlue ),
                      ),
                      focusedBorder: new OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(25.0),
                        borderSide:  BorderSide(color: Colors.lightBlue ),
                      ),
                      labelText: 'Name',
                      hintText: 'Enter your name'),
                  validator: (value){
                    name = value;
                    return null;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15.0, right: 15.0, top: 15, bottom: 15),
                //padding: EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      enabledBorder: new OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(25.0),
                        borderSide:  BorderSide(color: Colors.lightBlue ),
                      ),
                      focusedBorder: new OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(25.0),
                        borderSide:  BorderSide(color: Colors.lightBlue ),
                      ),
                      labelText: 'Phone',
                      hintText: 'Enter your phone'),
                  validator: (value){
                    phone = value;
                    return null;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15.0, right: 15.0, top: 15, bottom: 15),
                //TODO make this dropdown
                child: TextFormField(
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      enabledBorder: new OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(25.0),
                        borderSide:  BorderSide(color: Colors.lightBlue ),
                      ),
                      focusedBorder: new OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(25.0),
                        borderSide:  BorderSide(color: Colors.lightBlue ),
                      ),
                      labelText: 'Country',
                      hintText: 'Select your country'),
                  validator: (value){
                    country = value;
                    return null;
                  },
                ),
              ),
              Container(
                height: 50,
                width: 250,
                decoration: BoxDecoration(
                    color: Colors.blue, borderRadius: BorderRadius.circular(20)),
                child: FlatButton(
                  onPressed: () {
                    if(_formKey.currentState.validate()){
                      //Generate token for user
                      generateToken().then((value){
                        token_response = value;
                        Map<String,dynamic> parsedTokenResponse = jsonDecode(token_response);
                        if(parsedTokenResponse['token'] != ""){
                          //Register user on database
                          token = parsedTokenResponse['token'];
                          register().then((value)  {
                            signup_response = value;
                            Map<String,dynamic> parsedSignupResponse = jsonDecode(signup_response);
                            print(signup_response);
                            if(parsedSignupResponse["status"] == "success"){
                              Navigator.push(context, MaterialPageRoute(builder: (_) => LoginPage(title: 'Ecocup')));
                            }
                          });
                        }
                      });

                    }
                  },
                  child: Text(
                    'Register',
                    style: TextStyle(color: Colors.white, fontSize: 25),
                  ),
                ),
              ),
              SizedBox(
                height: 30,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15.0, right: 15.0, top: 5, bottom: 0),
                child: new GestureDetector(
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(builder: (_) => LoginPage(title: 'Ecocup')));
                  },
                  child: new Text("Already have an account? Login"),
                ),
              )
            ],
          ),
        ),
      )
    );
  }
}